var express = require('express'),
	app = express(),
	http = require('http').Server(app),
	io = require('socket.io')(http);

var voteData = [];

app.use('/public', express.static(__dirname + "/public"));
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
	res.sendfile(__dirname + '/public/index.html');
});

io.on('connection', function(socket){
	console.log('a user connected');

	io.emit('vote results', voteData);

	socket.on('new vote', function(data) {
		console.log('new vote');
		var elemName = data[0];
		
		var duplicateCheck = isItemInArray(voteData, data);

		if (duplicateCheck.status){
			voteData.splice(duplicateCheck.position, 1);
		}
		else {
			voteData.push(data);
		}
		
		io.emit('vote results', voteData);
	});

	socket.on('reset votes', function(data) {
		voteTotal = 0;
		for (key in voteData) {
			for (detail in voteData[key]) {
				voteData[key][detail] = 0;
			}
		}

		io.emit('vote results', voteData);
	});
});

function isItemInArray(array, item) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][0] == item[0] && array[i][1] == item[1]) {
            return {'status':true,'position':i};
        }
    }
    return {'status':false,'position':0};
}


http.listen(4000, function(){
	console.log('listening on *:4000');
});