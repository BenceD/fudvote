function channel() {
	let listeners = [];

	function removeListener(index) {
		return listeners.splice(index, 1);
	}

	function subscribe(listener) {
		listeners.push(listener);
		return function() {
			return removeListener(listeners.length - 1);
		};
	}

	function dispatch(data) {
		listeners.map(function(listener) {
			setTimeout(function() {
				listener(data);
			}, 0);
		});
	}

	return { subscribe, dispatch };
}

const mapEvents = {
	closeInfoWindows: channel(),
	updateMarkers: channel()
};
