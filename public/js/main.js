var socket = io();
var restaurants = ["Antalya Kebab", "Arriba Taqueria", "Bamba Marha Burger Bar", "Bel Frit", "Buddha Original", "Burger King", "El Bigote", "Frici papa", "Grill Pont", "Gringos Amigos", "hummusbar", "Imázs thai, japán és sushi étterem", "Istanbul török étterem", "Kerkyra Gyros Greek Food Bar", "KFC", "Lalibela Étterem", "Like Restaurant", "Meat & Sauce", "Most", "Mozsár", "Nokedli Kifőzde", "Pad Thai", "Padlizsánvirág", "Parázs Presszó", "pizza EATaliano", "Pizza Forte", "Pizza Hut Express", "Pizzica", "Sir Lancelot Lovagi Étterem", "Star Kebab Török Étterem", "SUPPE Bistro", "Titiz Turkish Restaurant", "Trófea Grill Restaurant", "Vidám kínai büfé", "Zing Burger" ];
var userName;

$.each(restaurants, function(i) {
    var elem = $('<div>').addClass('elem');
    
    $('<h3>').text(restaurants[i]).appendTo(elem);
    $('<div>').addClass('restaurant').addClass('well')
    .addClass('resta-'+i)
    .attr('data-elem', i)
    .appendTo(elem);

    elem.appendTo($('#wrapper'));
});

var newVote = function(data) {
    $.each(restaurants, function(i) {
        var target = $(".resta-" + i);
        target.html("");

        var count = countElement(i, data)[0];
        var counter = $('<span>').addClass('label').addClass('label-default').text(count);
        
        if(count == 0) {
            target.addClass('empty');
        }

        counter.appendTo(target);

        mapEvents.updateMarkers.dispatch({ name: restaurants[i], count });

        $.each(countElement(i, data)[1], function(index, val) {
            var elem = $('<span>').addClass('label').addClass('label-primary').text(val);
            elem.appendTo(target);
        });
    });
};

$(".restaurant").click(function(e) {
    e.preventDefault();
    var elemName = $(this).attr("data-elem");
    socket.emit("new vote", [elemName, userName]);
});

$("#toggle-empty").click(function(e) {
    e.preventDefault();
    $("html").toggleClass("hide-empty");
});

$('#login-form').submit(function () {
    $('html').addClass('login');
    userName = $("#username").val();
    localStorage.setItem("fudUserName", userName);

    return false;
});

window.addEventListener('load', function() {
    checkLogin();
}, false);

function checkLogin(){
    var storedUser = localStorage.getItem("fudUserName");
    if(storedUser) {
        userName = storedUser;
        $('html').addClass('login');
    }
    else {
        console.log("No user in localStorage!");
    }
}

socket.on('vote results', function(data) {
    console.log(data);
    newVote(data);
});

function countElement(item, array) {
    var count = 0;
    var names = [];
    $.each(array, function(i,v) {
        if (v[0] == item) {
            count++;
            names.push(v[1]);
        }
    });
    return [count, names];
}